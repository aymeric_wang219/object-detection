# README #

# Context of the project

Our team is preparing RoboCup@Work, a competition promoting research in robotics towards manufacturing purposes. While my teammates are developing a robotic arm, I am responsible for the development of the object detection script.

# What this repo is
This repo contains a first demo of a high-level script that will interact with our pre-trained deep neural network and extract positional data out of the centroids of bounding boxes.

Namely, this repo is composed of the following:

| File name  | Function |
| :---: | --- |
| `detection_v0.h` | High-level Python program interacting with the DNN |
| `yolov3.cfg` | Is a configuration file for the DNN defining its layers and weights |
| `yolov3.txt` | Contains the labels of the classification |
| `yolov3.weights` | Is the pre-trained DNN (YoloV3) |

Ideally, the final implementation of this algorithm will use the ROS framework and therefore is not standalone.

# Technical specifications

`detection_v0.h` is the fruit of my own software development however the `yolov3.*` are configuration files of YoloV3, an opensource DNN that is shows now (2020) the best tradeoff in performance and portability.