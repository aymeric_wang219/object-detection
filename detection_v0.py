#====================================#
#========= Library imports ==========#
#====================================#


from imutils.video import VideoStream
import time
import cv2
import copy
import numpy as np


#====================================#
#======= Function declaration =======#
#====================================#

def get_output_layers(net):
	layer_names = net.getLayerNames()
	output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
	return layer_names,output_layers

def draw_bounding_box(img, class_id, confidence, x, y, x_plus_w, y_plus_h):
	label = str(classes[class_id])
	color = COLORS[class_id]
	cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)
	cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


#====================================#
#======= Variable declaration =======#
#====================================#


weights='yolov3.weights'
objclasses='yolov3.txt'
config='yolov3.cfg'

with open(objclasses, 'r') as f:
    classes = [line.strip() for line in f.readlines()]
COLORS = np.random.uniform(0, 255, size=(len(classes), 3))


#====================================#
#=========== Main program ===========#
#====================================#

HEIGTH=480
WIDTH=640

conf_threshold = 1e-3
nms_threshold = 1e-3

inital_time = time.time()


# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")
vs = VideoStream(src=0).start()
#time.sleep(2.0)

net = cv2.dnn.readNet(weights, config)
found = []

while True:
	# grab the frame from the threaded video stream and resize it to
	# have a maximum width of 400 pixels
	CurrentImage = vs.read()
	found.clear()
	# defining 'q' key to quit loop
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break
	
	# read pre-trained model and config file
	
	blob = cv2.dnn.blobFromImage(image=CurrentImage, scalefactor=1/(9.9e-4*HEIGTH*WIDTH), size=(224,224), mean=(103.68,116.77,128.68), swapRB=True, crop=False)

	# set input blob for the network
	net.setInput(blob)
	# outs = net.forward()
	layer_names,output_layers=get_output_layers(net)
	outs = net.forward(output_layers)

	for out in outs:
		
		for detection in out:
			#print(len(detection))
			scores = detection[5:]
			class_id = np.argmax(scores)
			confidence = scores[class_id]
			#print(confidence)
			if confidence > 0.4:
				center_x = int(detection[0] * WIDTH)
				center_y = int(detection[1] * HEIGTH)
				w = int(detection[2] * WIDTH)
				h = int(detection[3] * HEIGTH)
				x = center_x - w/2
				y = center_y - h/2
				found.append
				draw_bounding_box(CurrentImage, class_id, confidence, round(x), round(y), round(x+w), round(y+h))
	#indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

	# go through the detections remaining
	# after nms and draw bounding box

	# display output image 
	cv2.imshow("Real Time Object Detection", CurrentImage)

	# wait until q key is pressed
	if cv2.waitKey(1) & 0xFF == ord('q'):
		print("[INFO] force quitting...")
		break

	# save output image to disk
	# cv2.imwrite("object-detection.jpg", CurrentImage)


cv2.destroyAllWindows()
vs.stop()
print("[INFO] end of program")
